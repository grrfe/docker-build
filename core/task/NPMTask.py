from typing import List

from core.task.BuildTask import BuildTask


class NPMTask(BuildTask):
    def __init__(self):
        super().__init__("npm")

    def build_cmd(self, _meta, directory) -> List[str]:
        npm_cmd = ["npm", "run"]
        npm_cmd.extend(_meta["npm_script"])

        return npm_cmd
