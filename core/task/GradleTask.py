import os
import sys
from typing import List

from core.task.BuildTask import BuildTask


class GradleTask(BuildTask):
    def __init__(self):
        super().__init__("gradle")

    def build_cmd(self, _meta, directory) -> List[str]:
        gradle_cmd = [os.path.join(directory, "gradlew.bat" if sys.platform == "win32" else "gradlew")]
        gradle_cmd.extend(_meta["gradle_task"])

        return gradle_cmd
