from abc import ABC
from typing import List


class BuildTask(ABC):
    def __init__(self, type_name):
        self.type_name = type_name

    def build_cmd(self, _meta, directory) -> List[str]:
        pass
