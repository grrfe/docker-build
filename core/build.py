import argparse
import json
import os
import subprocess
import sys
from typing import List

import docker
from git import Repo

from core.task.GradleTask import GradleTask
from core.task.NPMTask import NPMTask
from core.util.util import check_file, print_green, stream_logs


def run_cmd(command: List[str]):
    cmd_output = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                  universal_newlines=True, bufsize=0)
    for _line in cmd_output.stdout:
        print(_line.strip())


def replace_dict(_dict: dict, tag: str):
    for k, v in _dict.items():
        _dict[k] = v.replace("{tag}", tag)

    return _dict


__TASK_TYPES = {
    task.type_name: task for task in [
        NPMTask(),
        GradleTask()
    ]
}


def parse_input():
    parser = argparse.ArgumentParser(description="Build docker images")
    parser.add_argument("tag", help="Git tag that should be built")
    parser.add_argument("-r", "--repository", help="Repository directory", default=os.getcwd())

    args = parser.parse_args()

    meta_file_path = check_file(args.repository, "build_meta.json", "build_meta.json is missing!")
    check_file(args.repository, "Dockerfile", "Dockerfile is missing!")

    with open(meta_file_path, "r") as file:
        meta = json.load(file)

    git_tag = args.tag
    print_green(f"Checking out Git tag: {git_tag}")
    repo = Repo(args.repository)
    if repo.is_dirty():
        print("Repo is dirty, aborting..")
        sys.exit(1)

    checkout_result = str(repo.git.checkout(f"tags/{git_tag}"))

    print_green(f"Running task type: {meta['type']}")
    cmd = __TASK_TYPES[meta["type"]].build_cmd(meta, args.directory)
    run_cmd(cmd)

    image_name = meta["docker_image_name"]
    build_args = meta["docker_build_args"]

    tagged_image_name = f"{image_name}:{git_tag}"

    print_green(f"Building Docker image: {tagged_image_name}")
    client = docker.DockerClient(base_url="unix:///var/run/docker.sock")
    build_logs = client.api.build(decode=True,
                                  path=args.directory,
                                  tag=tagged_image_name,
                                  buildargs=replace_dict(build_args, git_tag))
    stream_logs(build_logs, "stream")

    print_green("Switching Git repo back")
    repo.git.switch("@{-1}")


if __name__ == "__main__":
    parse_input()
