import os
import sys

from colorama import Fore, Style


def print_green(text: str):
    print(f"{Fore.GREEN}{text}{Style.RESET_ALL}")


def check_file(path: str, error: str):
    if not os.path.exists(path):
        print(error)
        sys.exit(1)


def check_file(directory: str, file_name: str, error: str) -> str:
    file_path = os.path.join(directory, file_name)
    if not os.path.exists(file_path):
        print(error)
        sys.exit(1)

    return file_path


def stream_logs(logs, chunk_key: str):
    for chunk in logs:
        if chunk_key in chunk:
            for line in chunk[chunk_key].splitlines():
                print(line)
