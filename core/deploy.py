import argparse
import json
import os
import sys

import docker

from core.util.util import check_file, print_green, stream_logs


def parse_input():
    parser = argparse.ArgumentParser(description="Tag and deploy docker image to registry")
    parser.add_argument("tag", help="Docker image tag")
    parser.add_argument("registry", help="Docker registry name")
    parser.add_argument("-r", "--repository", help="Repository directory", default=os.getcwd())
    args = parser.parse_args()
    tag = args.tag

    meta_file_path = check_file(args.repository, "build_meta.json", "build_meta.json is missing!")

    with open(meta_file_path, "r") as file:
        meta = json.load(file)

    image_name = meta["docker_image_name"]

    repository = f"{args.registry}/{image_name}"
    print_green(f"Docker repository: {repository}")

    client = docker.DockerClient(base_url="unix:///var/run/docker.sock")

    print_green(f"Tagging image: {image_name}:{tag}")
    tag_success = client.api.tag(image_name, repository, tag)
    if not tag_success:
        print("Failed to tag image!")
        sys.exit(1)

    print_green(f"Pushing to {repository}:{tag}")
    push_logs = client.api.push(repository, tag, decode=True, stream=True)
    stream_logs(push_logs, "status")


if __name__ == "__main__":
    parse_input()
